package controllers

import play.api.mvc._
import scala.util.Random
import lib._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.json._
import play.api.cache.Cache
import play.api.Play.current

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 21/11/13
 * Time: 10:26
 * To change this template use File | Settings | File Templates.
 */
object Auth extends Controller with MongoController with MongoResponses with AuthHelper {
  def data = db.collection[JSONCollection]("mmaps")

  def login(sid: String) = Action(parse.json) { request =>
    def doLogin(as: AuthSession, user: JsObject): Result = {
      val pwd = (user \ "password").as[String]
      val correct = Crypto.hash(pwd + as.ssalt, "SHA-256")
      val provided = (request.body \ "password").as[String]

      if(correct == provided) {
        Cache.set(sid, ValidSession(sid, as.login, pwd, Crypto.hash(sid + pwd, "SHA-256")))
        Ok(Json.obj("success" -> 1))
      } else Unauthorized(Json.obj("success" -> 0, "error" -> "Access denied"))
    }

    Cache.getAs[AuthSession](sid).map { as =>
      Async {
        DbResponse(data.find(Json.obj("_id" -> as.login)).cursor[JsObject].toList()) {
          case Nil => NotFound(Json.obj("success" -> 0, "error" -> "User not found"))
          case a :: _ => doLogin(as, a)
        }
      }
    }.getOrElse(NotFound(Json.obj("success" -> 0, "error" -> "Session not found")))
  }

  def requestSession(login: String) = Action {
    val sid = Crypto.hash(Random.alphanumeric.take(64).mkString, "SHA-256")
    val ssalt = Random.alphanumeric.take(48).mkString

    def send(psalt: String) = {
      Cache.set(sid, AuthSession(login, ssalt))
      Ok(Json.obj(
        "session" -> sid,
        "salt1" -> psalt,
        "salt2" -> ssalt
      ))
    }

    Async {
      DbResponse(data.find(Json.obj("_id" -> login)).cursor[JsObject].toList()) {
        case Nil => NotFound(Json.obj("success" -> 0, "error" -> "User not found"))
        case a :: _ => send((a \ "salt").as[String])
      }
    }
  }

  def logout = AuthorizedAction { request => session =>
    session.invalidate
    Ok(Json.obj("success" -> 1))
  }
}

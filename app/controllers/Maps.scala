package controllers

import play.api.mvc._
import lib._
import play.modules.reactivemongo.json.collection.JSONCollection
import play.modules.reactivemongo.MongoController
import play.api.libs.json.{JsObject, Json}
import reactivemongo.bson.BSONObjectID

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 21/11/13
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
object Maps extends Controller with MongoController with MongoResponses with AuthHelper {
  def data = db.collection[JSONCollection]("mmaps")

  def list = AuthorizedAction { request => session =>
    Async {
      DbResponse(data.find(Json.obj("owner" -> session.login), Json.obj("_id" -> 1, "name" -> 1)).cursor[JsObject].toList) { res =>
        Ok(Json.obj("success" -> 1, "data" -> Json.toJson(res.map { obj =>
          Json.obj(
            "url" -> "http://%s/maps/m/%s".format(request.host, (obj \ "_id").as[String]),
            "name" -> (obj \ "name").as[String]
          ) })))
      }
    }
  }
  def delete(id: String) = AuthorizedAction { request => session =>
    Async {
      DbResponse(data.remove(Json.obj(
        "_id" -> id,
        "owner" -> session.login
      ), firstMatchOnly = true)) { e =>
        Ok(Json.obj("success" -> 1, "deleted" -> e.updated))
      }
    }
  }
  def get(id: String) = AuthorizedAction { request => session =>
    Async {
      DbResponse(data.find(
        Json.obj("_id" -> id, "owner" -> session.login),
        Json.obj("owner" -> 0, "_id" -> 0)).cursor[JsObject].toList) {
        case r :: _ => Ok(Json.obj("success" -> 1, "data" -> r))
        case _ => NotFound(Json.obj("success" -> 0, "error" -> "Not found"))
      }
    }
  }
  def set(id: String) = AuthorizedAction { request => session =>
    Async {
      DbResponse(data.find(
        Json.obj("_id" -> id, "owner" -> session.login),
        Json.obj("_id" -> 1)).cursor[JsObject].toList) {
        case r :: _ =>
          Async {
            DbResponse(data.save((request.body \ "data").as[JsObject] ++ Json.obj("_id" -> id, "owner" -> session.login))) { _ =>
              Ok(Json.obj("success" -> 1))
            }
          }
        case _ => NotFound(Json.obj("success" -> 0, "error" -> "Not found"))
      }
    }
  }
  def create = AuthorizedAction { request => session =>
    Async {
      val id = BSONObjectID.generate
      DbResponse(data.save(Json.obj(
        "_id" -> id.stringify, "owner" -> session.login, "name" -> (request.body \ "name").as[String]))) { _ =>
          Ok(Json.obj("success" -> 1, "url" -> "http://%s/maps/m/%s".format(request.host, id.stringify) )) }
    }
  }
}

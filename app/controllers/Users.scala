package controllers

import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.json._
import lib._
import scala.util.Random

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 21/11/13
 * Time: 10:26
 * To change this template use File | Settings | File Templates.
 */
object Users extends Controller with MongoController with MongoResponses with AuthHelper {
  def data = db.collection[JSONCollection]("mmaps")

  def create = Action(parse.json) { request =>
    Async {
      val body = request.body
      val salt = Random.alphanumeric.take(48).mkString
      val json = Json.obj(
        "_id" -> (body \ "username").as[String],
        "password" -> Crypto.hash((body \ "password").as[String] + salt, "SHA-256"),
        "salt" -> salt
      )
      DbResponse(data.insert(json))(_ => Ok(Json.obj("success" -> 1)))
    }
  }
  def delete = AuthorizedAction { request => session =>
    Async {
      DbResponse(data.remove(Json.obj("$or" -> Json.arr(
        Json.obj("_id" -> session.login),
        Json.obj("owner" -> session.login)
      )), firstMatchOnly = true)) { _ =>
        session.invalidate
        Ok(Json.obj("success" -> 1))
      }
    }
  }
}

package lib

import java.security.MessageDigest

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 22/11/13
 * Time: 12:47
 * To change this template use File | Settings | File Templates.
 */
object Crypto {
  def hash(s: String, alg: String): String = MessageDigest.getInstance(alg).digest(s getBytes).map("%02x" format _).mkString
}

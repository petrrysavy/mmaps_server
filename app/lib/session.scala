package lib

import play.api.cache.Cache
import play.api.Play.current

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 22/11/13
 * Time: 18:19
 * To change this template use File | Settings | File Templates.
 */

abstract class Session
case class AuthSession(login: String, ssalt: String) extends Session
case class ValidSession(id: String, login: String, password: String, var token: String) extends Session {
  def invalidate = Cache.remove(id)
}

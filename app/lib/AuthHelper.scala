package lib

import play.api.mvc._
import play.api.mvc.BodyParsers._
import play.api.Play.current
import play.api.cache.Cache
import play.api.mvc.Results.Status
import play.api.libs.json.{JsValue, Json}
import play.api.Logger

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 23/11/13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */
trait AuthHelper {
  val unauthorized = new Status(401)(Json.obj("success" -> 0, "error" -> "Unauthorized"))
  def AuthorizedAction(worker: Request[JsValue] => ValidSession => Result) = {
    Action(parse.json) { request =>
      val session = Cache.getAs[ValidSession]((request.body \ "session").as[String])
      session.map { s =>
        if(s.token == (request.body \ "token").as[String]) {
          s.token = Crypto.hash(s.token + s.password, "SHA-256")
          worker(request)(s)
        } else unauthorized
      }.getOrElse(unauthorized)
    }
  }
}

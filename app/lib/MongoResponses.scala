package lib

import reactivemongo.core.commands.LastError
import play.api.mvc._
import play.api.libs.json.Json
import play.api.mvc.Results.Status
import scala.concurrent._
import ExecutionContext.Implicits.global

/**
 * Created with IntelliJ IDEA.
 * User: wigos
 * Date: 22/11/13
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
trait MongoResponses {
  def DbResponse[T](p: Future[T])(otherwise: T => Result): Future[Result] =
    p.recoverWith { case ex => future{ Failed(ex.getMessage) } }.map {
      case Failed(msg) => new Status(418)(Json.obj("success" -> 0, "error" -> msg))
      case le: LastError if !le.ok => new Status(418)(Json.obj("success" -> 0, "error" -> le.stringify))
      case a => otherwise(a.asInstanceOf[T])
    }
}

case class Failed(msg: String)
